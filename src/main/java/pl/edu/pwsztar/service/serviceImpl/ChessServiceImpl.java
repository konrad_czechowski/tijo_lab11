package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop, knight, king, queen, rock, pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rock = rock;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        String[] positionStart = figureMoveDto.getStart().split("_");
        String[] positionDest = figureMoveDto.getDestination().split("_");

        int xStart = positionStart[0].charAt(0) - 96;
        int yStart = Integer.parseInt(positionStart[1]);
        int xEnd = positionDest[0].charAt(0) - 96;
        int yEnd = Integer.parseInt(positionDest[1]);

        switch (figureMoveDto.getType()) {
            case KING:
                return king.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case PAWN:
                return pawn.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KNIGHT:
                return knight.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case ROCK:
                return rock.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case QUEEN:
                return queen.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case BISHOP:
                return bishop.isCorrectMove(xStart, yStart, xEnd, yEnd);
        }
        return false;
    }
}
